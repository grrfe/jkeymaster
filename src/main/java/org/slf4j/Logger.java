package org.slf4j;


public class Logger {
    public void debug(String s, Exception ex) {
    }

    public void debug(String str, Object... args) {
    }

    public void trace(String str, Object... args) {
    }

    public void info(String str, Object... args) {
    }

    public void warn(String str, Object... args) {
    }

    public void error(String str, Object... args) {
    }

    public void log(String prefix, String str, Object... args) {
    }

    public boolean isDebugEnabled() {
        return false;
    }

    public boolean isTraceEnabled() {
        return false;
    }


}
