pluginManagement {
    repositories {
        mavenLocal()
        maven(url = "https://jitpack.io")
        gradlePluginPortal()
    }

    plugins {
        id("net.nemerosa.versioning") version "3.0.0"
        id("de.fayard.refreshVersions") version "0.60.3"
        id("org.gradle.toolchains.foojay-resolver-convention") version "0.7.0"
    }
}


plugins {
    id("org.gradle.toolchains.foojay-resolver-convention")
    id("de.fayard.refreshVersions")
}


rootProject.name = "jkeymaster"
