plugins {
    `java-library`
    `maven-publish`
    id("net.nemerosa.versioning")
}

repositories {
    mavenLocal()
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    api(libs.net.java.dev.jna.jna.jpms)
    api(libs.net.java.dev.jna.jna.platform.jpms)
    api(libs.dbus.java)
}


group = "com.tulskiy.keymaster"
version = versioning.info.tag ?: versioning.info.full

description = "jkeymaster"

java {
    withSourcesJar()
    withJavadocJar()

    toolchain {
        languageVersion.set(JavaLanguageVersion.of(21))
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
